-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: glib2.0
Binary: libglib2.0-0, libglib2.0-tests, libglib2.0-udeb, libglib2.0-bin, libglib2.0-dev, libglib2.0-0-dbg, libglib2.0-data, libglib2.0-doc, libgio-fam
Architecture: any all
Version: 2.50.3-2.0tails1
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Iain Lane <laney@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: http://www.gtk.org/
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/glib2.0/
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/glib2.0/
Testsuite: autopkgtest
Testsuite-Triggers: build-essential, dbus, dbus-x11, gnome-desktop-testing, xauth, xvfb
Build-Depends: debhelper (>= 9.20141010), cdbs (>= 0.4.93), dh-autoreconf, dh-exec, pkg-config (>= 0.16.0), gettext, autotools-dev, gnome-pkg-tools (>= 0.11), dpkg-dev (>= 1.17.14), libelf-dev (>= 0.142), libmount-dev (>= 2.28) [linux-any], libpcre3-dev (>= 1:8.35), gtk-doc-tools (>= 1.20), libselinux1-dev [linux-any], linux-libc-dev [linux-any], libgamin-dev [!linux-any] | libfam-dev [!linux-any], zlib1g-dev, desktop-file-utils <!nocheck>, dbus <!nocheck>, shared-mime-info <!nocheck>, tzdata <!nocheck>, xterm <!nocheck>, python3:any (>= 2.7.5-5~), python3-dbus <!nocheck>, python3-gi <!nocheck>, libxml2-utils, xsltproc, docbook-xml, docbook-xsl, libffi-dev (>= 3.0.0)
Package-List:
 libgio-fam deb libs optional arch=hurd-any,kfreebsd-any
 libglib2.0-0 deb libs optional arch=any
 libglib2.0-0-dbg deb debug extra arch=any
 libglib2.0-bin deb misc optional arch=any
 libglib2.0-data deb libs optional arch=all
 libglib2.0-dev deb libdevel optional arch=any
 libglib2.0-doc deb doc optional arch=all
 libglib2.0-tests deb libs optional arch=any
 libglib2.0-udeb udeb debian-installer optional arch=any
Checksums-Sha1:
 3177471fb11fd2fd5abb77ec0675ee0049521009 7589284 glib2.0_2.50.3.orig.tar.xz
 8988ce7825445cb6fc85d2c42a232cba2561c763 71432 glib2.0_2.50.3-2.0tails1.debian.tar.xz
Checksums-Sha256:
 82ee94bf4c01459b6b00cb9db0545c2237921e3060c0b74cff13fbc020cfd999 7589284 glib2.0_2.50.3.orig.tar.xz
 7a2690420dcb315ad2b06bc25558b24db25a5958c7388bb6620567006ee12140 71432 glib2.0_2.50.3-2.0tails1.debian.tar.xz
Files:
 381ab22934f296750d036aa55a397ded 7589284 glib2.0_2.50.3.orig.tar.xz
 25d0bbdef273a891dc919701887dd325 71432 glib2.0_2.50.3-2.0tails1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEUrafEKOweFrQWvtHHYTM8BDMW8cFAlmxZbsSHGFub255bUBy
aXNldXAubmV0AAoJEB2EzPAQzFvHvSQP/3IZ4qFA55SR2GJRoxf5MC0VX2lsUxr6
DwV76h4oTO+O9DMaa0GfuNYeZ3KFspSHmzxb01U/of2MwCgSp4Yt111WoIIE4p3S
TwrRUdzEo5T2YGibS9njHMFiZHk65uVomF0ad6bwnK/9ioO+xG5eGlEG/VJ7jrl+
GPvxwADnwof1dm66ClD+Ga9MVd0YmxIwq2iXaXPOIJr1WvFB2H+/JMW1LIwI3E0B
WiM5q1iD5PywYGwf8DkzGu0h2m794yaK0/U2m6OG7KE/p6zN1eaJOVYn4PWsMjqs
0plsoig5+pPWM2vLL1BFdModu4HIiaCUn9I+8tT5Kw4+Nee7EwbuyVdKQYcSJUhz
hG7gJU/uk8+By40GrNj3VFpDDCRnJeALqxwVTPOsfBWyDOf5nL2VKLjLbffa6W+8
4ES1FgILZaoocq/XhcNUir626Nipsa1DjJYtSUw6wga29mV7aFNj9twAt5lAnvm+
hHm5M0R/F40LUlcNXrLYYkqy9CpzwdY+/kmYGoWQAOqUo1l9WLwG+vGltrlQ2TQw
iV9P4eyTNCHgRRp0kHh7c+D6522DwwYf0c8XSJzhiI7r85y0avxdndu4cK1XWe4X
5dgY+8+akinfZjEncYd78P41RVvC7AolAqDK/WdaQ32byhttLGQLBAzNqulctjkd
61agGcZvycGF
=AyZg
-----END PGP SIGNATURE-----
