From bdbc8d0097bc3003115e81d98e81d8ef5ebbfd50 Mon Sep 17 00:00:00 2001
From: segfault <segfault@riseup.net>
Date: Fri, 9 Mar 2018 00:40:17 +0100
Subject: [PATCH] Support TCRYPT volumes

This is a combination of 3 commits:

Add G_ASK_PASSWORD_TCRYPT flag

Support VeraCrypt PIM property in GMountOperation

Support TCRYPT hidden and system volume in GMountOperation
---
 gio/gioenums.h        |  12 +--
 gio/gmountoperation.c | 189 +++++++++++++++++++++++++++++++++++++++++-
 gio/gmountoperation.h |  15 ++++
 3 files changed, 210 insertions(+), 6 deletions(-)

diff --git a/gio/gioenums.h b/gio/gioenums.h
index bc844b9b9..b9dbf10b2 100644
--- a/gio/gioenums.h
+++ b/gio/gioenums.h
@@ -572,17 +572,19 @@ typedef enum {
  * @G_ASK_PASSWORD_NEED_DOMAIN: operation requires a domain.
  * @G_ASK_PASSWORD_SAVING_SUPPORTED: operation supports saving settings.
  * @G_ASK_PASSWORD_ANONYMOUS_SUPPORTED: operation supports anonymous users.
+ * @G_ASK_PASSWORD_TCRYPT: operation takes TCRYPT parameters
  *
  * #GAskPasswordFlags are used to request specific information from the
  * user, or to notify the user of their choices in an authentication
  * situation.
  **/
 typedef enum {
-  G_ASK_PASSWORD_NEED_PASSWORD       = (1 << 0),
-  G_ASK_PASSWORD_NEED_USERNAME       = (1 << 1),
-  G_ASK_PASSWORD_NEED_DOMAIN         = (1 << 2),
-  G_ASK_PASSWORD_SAVING_SUPPORTED    = (1 << 3),
-  G_ASK_PASSWORD_ANONYMOUS_SUPPORTED = (1 << 4)
+  G_ASK_PASSWORD_NEED_PASSWORD           = (1 << 0),
+  G_ASK_PASSWORD_NEED_USERNAME           = (1 << 1),
+  G_ASK_PASSWORD_NEED_DOMAIN             = (1 << 2),
+  G_ASK_PASSWORD_SAVING_SUPPORTED        = (1 << 3),
+  G_ASK_PASSWORD_ANONYMOUS_SUPPORTED     = (1 << 4),
+  G_ASK_PASSWORD_TCRYPT                  = (1 << 5),
 } GAskPasswordFlags;
 
 
diff --git a/gio/gmountoperation.c b/gio/gmountoperation.c
index 2a2b4ce5b..aac3c07da 100644
--- a/gio/gmountoperation.c
+++ b/gio/gmountoperation.c
@@ -68,6 +68,9 @@ struct _GMountOperationPrivate {
   gboolean anonymous;
   GPasswordSave password_save;
   int choice;
+  gboolean hidden_volume;
+  gboolean system_volume;
+  int pim;
 };
 
 enum {
@@ -77,7 +80,10 @@ enum {
   PROP_ANONYMOUS,
   PROP_DOMAIN,
   PROP_PASSWORD_SAVE,
-  PROP_CHOICE
+  PROP_CHOICE,
+  PROP_HIDDEN_VOLUME,
+  PROP_SYSTEM_VOLUME,
+  PROP_PIM
 };
 
 G_DEFINE_TYPE_WITH_PRIVATE (GMountOperation, g_mount_operation, G_TYPE_OBJECT)
@@ -124,6 +130,21 @@ g_mount_operation_set_property (GObject      *object,
                                     g_value_get_int (value));
       break;
 
+    case PROP_HIDDEN_VOLUME:
+      g_mount_operation_set_hidden_volume (operation,
+                                           g_value_get_boolean (value));
+      break;
+
+    case PROP_SYSTEM_VOLUME:
+      g_mount_operation_set_system_volume (operation,
+                                           g_value_get_boolean (value));
+      break;
+
+    case PROP_PIM:
+        g_mount_operation_set_pim (operation,
+                                   g_value_get_int (value));
+        break;
+
     default:
       G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
       break;
@@ -169,6 +190,18 @@ g_mount_operation_get_property (GObject    *object,
       g_value_set_int (value, priv->choice);
       break;
 
+    case PROP_HIDDEN_VOLUME:
+      g_value_set_boolean (value, priv->hidden_volume);
+      break;
+
+    case PROP_SYSTEM_VOLUME:
+      g_value_set_boolean (value, priv->system_volume);
+      break;
+
+    case PROP_PIM:
+      g_value_set_int (value, priv->pim);
+      break;
+
     default:
       G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
       break;
@@ -504,6 +537,48 @@ g_mount_operation_class_init (GMountOperationClass *klass)
                                                      0, G_MAXINT, 0,
                                                      G_PARAM_READWRITE|
                                                      G_PARAM_STATIC_NAME|G_PARAM_STATIC_NICK|G_PARAM_STATIC_BLURB));
+
+  /**
+   * GMountOperation:hidden_volume:
+   *
+   * Whether the device to be unlocked is a TCRYPT hidden volume.
+   */
+  g_object_class_install_property (object_class,
+                                   PROP_HIDDEN_VOLUME,
+                                   g_param_spec_boolean ("hidden-volume",
+                                                         P_("Hidden Volume"),
+                                                         P_("Whether to unlock a hidden volume"),
+                                                         FALSE,
+                                                         G_PARAM_READWRITE|
+                                                         G_PARAM_STATIC_NAME|G_PARAM_STATIC_NICK|G_PARAM_STATIC_BLURB));
+
+  /**
+  * GMountOperation:system_volume:
+  *
+  * Whether the device to be unlocked is a TCRYPT system volume.
+  */
+  g_object_class_install_property (object_class,
+                                   PROP_SYSTEM_VOLUME,
+                                   g_param_spec_boolean ("system-volume",
+                                                         P_("System Volume"),
+                                                         P_("Whether to unlock a system volume"),
+                                                         FALSE,
+                                                         G_PARAM_READWRITE|
+                                                         G_PARAM_STATIC_NAME|G_PARAM_STATIC_NICK|G_PARAM_STATIC_BLURB));
+
+  /**
+  * GMountOperation:pim:
+  *
+  * The VeraCrypt PIM value, when unlocking a VeraCrypt volume.
+  */
+  g_object_class_install_property (object_class,
+                                   PROP_PIM,
+                                   g_param_spec_int ("pim",
+                                                     P_("PIM"),
+                                                     P_("The VeraCrypt PIM"),
+                                                     0, G_MAXINT, 0,
+                                                     G_PARAM_READWRITE|
+                                                     G_PARAM_STATIC_NAME|G_PARAM_STATIC_NICK|G_PARAM_STATIC_BLURB));
 }
 
 static void
@@ -736,6 +811,118 @@ g_mount_operation_set_choice (GMountOperation *op,
     }
 }
 
+/**
+ * g_mount_operation_get_hidden_volume:
+ * @op: a #GMountOperation.
+ *
+ * Check to see whether the mount operation is being used
+ * for a TCRYPT hidden volume.
+ *
+ * Returns: %TRUE if mount operation is for hidden volume.
+ **/
+gboolean
+g_mount_operation_get_hidden_volume (GMountOperation *op)
+{
+  g_return_val_if_fail (G_IS_MOUNT_OPERATION (op), FALSE);
+  return op->priv->hidden_volume;
+}
+
+/**
+ * g_mount_operation_set_hidden_volume:
+ * @op: a #GMountOperation.
+ * @hidden_volume: boolean value.
+ *
+ * Sets the mount operation to use a hidden volume if @hidden_volume is %TRUE.
+ **/
+void
+g_mount_operation_set_hidden_volume (GMountOperation *op,
+				 gboolean         hidden_volume)
+{
+  GMountOperationPrivate *priv;
+  g_return_if_fail (G_IS_MOUNT_OPERATION (op));
+  priv = op->priv;
+
+  if (priv->hidden_volume != hidden_volume)
+    {
+      priv->hidden_volume = hidden_volume;
+      g_object_notify (G_OBJECT (op), "hidden_volume");
+    }
+}
+
+/**
+ * g_mount_operation_get_system_volume:
+ * @op: a #GMountOperation.
+ *
+ * Check to see whether the mount operation is being used
+ * for a TCRYPT system volume.
+ *
+ * Returns: %TRUE if mount operation is for system volume.
+ **/
+gboolean
+g_mount_operation_get_system_volume (GMountOperation *op)
+{
+  g_return_val_if_fail (G_IS_MOUNT_OPERATION (op), FALSE);
+  return op->priv->system_volume;
+}
+
+/**
+ * g_mount_operation_set_system_volume:
+ * @op: a #GMountOperation.
+ * @system_volume: boolean value.
+ *
+ * Sets the mount operation to use a system volume if @system_volume is %TRUE.
+ **/
+void
+g_mount_operation_set_system_volume (GMountOperation *op,
+				 gboolean         system_volume)
+{
+  GMountOperationPrivate *priv;
+  g_return_if_fail (G_IS_MOUNT_OPERATION (op));
+  priv = op->priv;
+
+  if (priv->system_volume != system_volume)
+    {
+      priv->system_volume = system_volume;
+      g_object_notify (G_OBJECT (op), "system_volume");
+    }
+}
+
+/**
+ * g_mount_operation_get_pim:
+ * @op: a #GMountOperation.
+ *
+ * Gets a pim from the mount operation.
+ *
+ * Returns: The VeraCrypt PIM within @op.
+ **/
+int
+g_mount_operation_get_pim (GMountOperation *op)
+{
+  g_return_val_if_fail (G_IS_MOUNT_OPERATION (op), 0);
+  return op->priv->pim;
+}
+
+/**
+ * g_mount_operation_set_pim:
+ * @op: a #GMountOperation.
+ * @pim: an integer.
+ *
+ * Sets the mount operation's pim to @pim.
+ **/
+void
+g_mount_operation_set_pim (GMountOperation *op,
+			      int            pim)
+{
+  GMountOperationPrivate *priv;
+  g_return_if_fail (G_IS_MOUNT_OPERATION (op));
+  priv = op->priv;
+  if (priv->pim != pim)
+    {
+      priv->pim = pim;
+      g_object_notify (G_OBJECT (op), "pim");
+    }
+}
+
 /**
  * g_mount_operation_reply:
  * @op: a #GMountOperation
diff --git a/gio/gmountoperation.h b/gio/gmountoperation.h
index 24b96e002..eefd7cf08 100644
--- a/gio/gmountoperation.h
+++ b/gio/gmountoperation.h
@@ -149,6 +149,21 @@ void          g_mount_operation_set_choice        (GMountOperation *op,
 GLIB_AVAILABLE_IN_ALL
 void          g_mount_operation_reply             (GMountOperation *op,
 						   GMountOperationResult result);
+GLIB_AVAILABLE_IN_ALL
+gboolean      g_mount_operation_get_hidden_volume (GMountOperation *op);
+GLIB_AVAILABLE_IN_ALL
+void          g_mount_operation_set_hidden_volume (GMountOperation *op,
+						   gboolean         hidden_volume);
+GLIB_AVAILABLE_IN_ALL
+gboolean      g_mount_operation_get_system_volume (GMountOperation *op);
+GLIB_AVAILABLE_IN_ALL
+void          g_mount_operation_set_system_volume (GMountOperation *op,
+						   gboolean         system_volume);
+GLIB_AVAILABLE_IN_ALL
+int           g_mount_operation_get_pim        (GMountOperation *op);
+GLIB_AVAILABLE_IN_ALL
+void          g_mount_operation_set_pim        (GMountOperation *op,
+int              pim);
 
 G_END_DECLS
 
-- 
2.17.1

